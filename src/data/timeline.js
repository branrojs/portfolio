export default [
    {
        year: 'Today',
        title: 'Mid Web Developer at Accenture.',
        duration: '3.6 years',
        details: 'I helped to build different enterprise applications for 500 Fortune companies, every project has its team working with Agile setting. The tech stack cosisted in diferent languages and tools suck as Java, Javascript, Webpack, Vue, AEM, Jest, and React.',
    },
    {
        year: 'Today',
        title: "Bachelor's Degree Software Engineering",
        duration: '3 years',
        details: "I'm pursuing the knowledge that a computer science degree has to offer. By now I'm learning about computer architecture, computer science theory, an array of programming and different aspects of it.",
    },
    {
        year: '2018',
        title: "Full Stack Developer",
        duration: '1.5 years',
        details: "I helped build an eParticipation website for a regional government institution, its tech stack consisted of Ruby, Rails, Javascript (React), PostgreSQL, and other document management application for a mail institution, tech stack was Java, Springboot, Angular, PostgreSQL and handful of other languages and tools",
    },
    {
        year: '2017',
        title: "Full Stack Developer",
        duration: '5 months',
        details: "I helped customize a gym resources management application for different customers, the tech stack for this included Visual Basic and SQL server. Also did some customizations to a sales management application for a local furniture company, this app included tech stack as PHP, Jquery, and MySQL.",
    },
]