import React from 'react'

function Intro() {
    return (
        <div className='flex items-center justify-center flex-col text-center pt-20 pb-6'>
            <h1 className='text-4xl md:text-7xl mb-1 md:mb-3 dark:text-white font-bold'>Brandon Rojas Sanabria</h1>
            <p className='texst-base md:text-xl mb-3 font-medium'>Software Engineer & Web Developer</p>
            <p className='texst-sm max-w-xl mb-6 font-bold'>I'm a Full-stack developer from Costa Rica with 4 years of experience in the role, I love to try new technologies and implement them in personal projects. I have experience in Javascript (react, vue) and Java mainly, but I've worked with Ruby (RoR), AEM, and Python.
            <br />
            According to Rosetta Stone, I'm C1+ in my English Skills, but who knows, that's just a score :D nowadays I still pause when I'm trying to remember how to say beer and not bird or bear (just a joke). Oh and I'm also learning Portuguese. 
            <br />
            When I'm not coding, I love to do a lot of things, CrossFit, books, videogames, movies, you know the classic 'I like to do a lot and more but my time and energy are limited'</p>
        </div>
    )
}

export default Intro;