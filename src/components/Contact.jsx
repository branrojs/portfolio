import React from 'react'
import Title from './Title';
import { SocialIcon }  from 'react-social-icons';
import contactLinks from '../data/contactLinks';

function Contact() {
    return (
        <div className="grid  justify-center mb-10 mx-auto">
            <div className='grid grid-cols-2 gap-4'>
                <div className='col-span-2 mx-auto'>
                    <Title>Social Media</Title>
                </div>
                {contactLinks.map( link => (
                    <div className='col-span-1 mx-auto'>
                        <SocialIcon url={link} />
                    </div>
                ))}
            </div>
        </div>
    )
}

export default Contact;