import React, { useEffect, useState}  from 'react'
/* components */
import Contact from './components/Contact'
import Footer from './components/Footer'
import Intro from './components/Intro'
import Timeline from './components/Timeline'
import Portfolio from './components/Portfolio'
/* hooks */
import useThemeHandler from './hooks/useThemeHandler'

function App() {

  const { sun, moon, theme, handleThemeSwitch } = useThemeHandler();

  return (
    <>
      <button
        type="button"
        onClick={handleThemeSwitch}
        className="fixed p-2 z-10 right-20 top-4 bg-violet-300 dark:bg-orange-300 text-lg rounded-md"
      >
        {theme === 'dark' ? sun : moon}
      </button>
      <div className="bg-white dark:bg-stone-900 text-stone-900 dark:text-stone-300 min-h-screen font-inter">
        <div className="max-w-5xl w-11/12 mx-auto">
          <Intro />
          <Portfolio />
          <Timeline/> 
          <Contact/>
          <Footer/>
        </div>
      </div>
    </>
  )
}

export default App;
